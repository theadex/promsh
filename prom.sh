#!/bin/bash
#
# prom.sh is a shell library to help write metrics in the Prometheus
# exposition format.
#
# Copyright 2019 The Adex GmbH - www.theadex.com
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

declare -A prom_gauges
declare -A prom_counters
declare -A prom_help

prom_script_start=$(date +%s)
if [[ -z "$(trap -p EXIT)" ]]; then
    trap 'prom-atexit' EXIT
fi
if [[ -z "$(trap -p ERR)" ]]; then
    trap 'prom-atexit' ERR
fi

[[ -n "${prom_script-}" ]] || prom_script=$(basename "$0" | sed 's/[^A-Za-z0-9]/_/g')
[[ -n "${prom_textfiledir-}" ]] || prom_textfiledir=/var/lib/node_exporter

# prom-init initializes the Prometheus expositor with a custom script
# name. In most cases this should not be necessary; it is already
# initialized with $0 as the script name.
prom-init() {
    prom_script=${1:-$prom_script}
    prom_script_start=$(date +%s)
}

# prom-atexit should be called before your script exits to mark its stop
# timestamp and do a final export. Doing this explicitly is only
# necessary if you install your own EXIT trap.
#
# If you pass an argument it will be used as script's exit code,
# otherwise the value of $? is used.
prom-atexit() {
    prom_script_exit_code=${1:-$?}
    prom_script_stop=$(date +%s)
    prom-export
}

# prom-def attaches a descriptive message to a metric.
prom-def() {
    local name=$1; shift;
    prom_help[$name]="$*"
}

# prom-export exports the metric to the standard file.
prom-export() {
    local exportfile=${prom_exportfile:-$prom_textfiledir/$prom_script.prom}
    if [[ $exportfile == "-" ]]; then
        prom-expose
    elif [[ $exportfile != "/dev/null" ]]; then
        local tmp
        tmp=$(mktemp "$exportfile.XXXXXX")
        prom-expose > "$tmp"
        chmod 644 "$tmp"
        mv -f "$tmp" "$exportfile"
        rm -f "$tmp"
    fi
}

# prom-expose echos all metrics to stdout.
prom-expose() {
    set +x
    echo "# HELP promsh_start_timestamp Start time of a script using prom.sh"
    echo "# TYPE promsh_start_timestamp gauge"
    echo "promsh_start_timestamp{script=\"$prom_script\"} $prom_script_start"

    if [[ -n "${prom_script_stop-}" ]]; then
        echo "# HELP promsh_stop_timestamp Exit time of a script using prom.sh"
        echo "# TYPE promsh_stop_timestamp gauge"
        echo "promsh_stop_timestamp{script=\"$prom_script\"} $prom_script_stop"
        echo "# HELP promsh_duration_seconds Duration of a prom.sh script execution"
        echo "# TYPE promsh_duration_seconds gauge"
        echo "promsh_duration_seconds{script=\"$prom_script\"}" $((prom_script_stop - prom_script_start))
        echo "# HELP promsh_script_exit_code Exit code of a script using prom.sh"
        echo "# TYPE promsh_script_exit_code gauge"
        echo "promsh_script_exit_code{script=\"$prom_script\"}" "${prom_script_exit_code:--1}"
    fi

    local metric

    for metric in $(xargs -n 1 <<< "${!prom_gauges[@]}" | sort); do
        value="${prom_gauges[$metric]}"
        [[ -z "${prom_help[$metric]-}" ]] \
            || echo "# HELP $metric ${prom_help[$metric]}"
        echo "# TYPE $metric gauge"
        echo "$metric $value"
    done

    for metric in $(xargs -n 1 <<< "${!prom_counters[@]}" | sort); do
        value="${prom_counters[$metric]}"
        [[ -z "${prom_help[$metric]-}" ]] \
            || echo "# HELP $metric ${prom_help[$metric]}"
        echo "# TYPE $metric counter"
        echo "$metric $value"
    done
}

# prom-set sets a gauge to the given value.
prom-set() {
    prom_gauges[$1]=$2
}

# prom-now sets a gauge to the current timestamp in seconds.
prom-now() {
    prom_gauges[$1]=$(date +%s)
}

# prom-inc increments a counter metric by the provided value, or 1 if no
# value is provided.
prom-inc() {
    prom_counters[$1]=$((${prom_counters[$1]:-0} + ${2:-1}))
}

# prom-time times the execution of a command under the given metric
# name, and records its start and stop timestamps. prom-time is not
# available as "prom time" because it cannot be no-oped out.
prom-time() {
    local metric=$1; shift
    prom-def "${metric}_start_timestamp" "Start time of running \`$*\`"
    prom-def "${metric}_stop_timestamp" "Stop time of running \`$*\`"
    prom-def "${metric}_duration_seconds" "Total time to run \`$*\`"
    prom-def "${metric}_exit_code" "Last exit code code of \`$*\`"
    prom_gauges["${metric}_start_timestamp"]=$(date +%s)
    prom-export
    local x=0; "$@" || x=$?
    prom_gauges["${metric}_exit_code"]=$x
    prom_gauges["${metric}_stop_timestamp"]=$(date +%s)
    prom_gauges["${metric}_duration_seconds"]=$((prom_gauges["${metric}_stop_timestamp"] - prom_gauges["${metric}_start_timestamp"]))
    return $x
}

# prom wraps prom.sh commands to make no-op aliasing easier when prom.sh
# is not available.
prom() {
    local op=$1; shift
    case $op in
        init) prom-init "$@" ;;
        inc) prom-inc "$@" ;;
        set) prom-set "$@" ;;
        now) prom-now "$@" ;;
        def) prom-def "$@" ;;
        export) prom-export "$@" ;;
        expose) prom-expose "$@" ;;
        atexit) prom-atexit "$@" ;;
        *)
            echo "invalid prom.sh command: $op" >&2
            return 1
    esac
}
