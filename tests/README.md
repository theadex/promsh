# prom.sh test suite

A prom.sh test case is a `script` which generates some output, possibly
including metrics, and an `expected.prom` file containing the final
expected metrics exposition sans timestamps.
