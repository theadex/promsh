# prom.sh

prom.sh is a shell library to assist generating Prometheus exposition
files for its textfile exporter from bash scripts.

To use it, source the file at the start of your script:

```sh
#!/bin/bash

source /usr/local/lib/prometheus/prom.sh
```

To configure the script name or output file, set `prom_script` and
`prom_textfiledir` before sourcing `prom.sh`. By default it writes
world-readable metrics expositions to `/var/lib/node_exporter`.


## `prom`

Most metrics can be set using the `prom` wrapper.

- `prom def metric help-string` - attach a help string to a metric; this
  is optional but recommended
- `prom set metric value` - set the gauge named metric to the given
  value
- `prom inc metric [delta]` - increment the counter named by the metric,
  by 1 if no value is given (use `prom inc … 0` to initialize a
  zero-value counter)
- `prom now metric` - set the gauge named by the metric to the current
  time in seconds


## `prom-time metric-prefix command …`

`prom-time` takes a metric prefix and a command with arguments, marks
the start time and exports its metrics, then runs the command, and
records its stop time and duration as metrics.


## `prom expose` and `prom export`

Exposing metrics means to print them to standard output; exporting the
metrics means to write them to the standard export file. This can be
controlled by setting the `prom_textfiledir` or `prom_exportfile`
variable before sourcing the script.

You only need to run this manually if you want to export metrics while
your script is executing.

As a special case, `prom export` understands `/dev/null` to mean skip
export and `-` to mean the same as `prom expose`.


## `prom init [script-name]` and `prom atexit`

`init` resets the script start time and allows you to override the
service name attached to some labels (by default it is the script name).
You can also override it by setting `prom_script` before sourcing
prom.sh.

`atexit` marks the script end time and performs a final export. This
happens by default and only needs to be used if you `trap` `EXIT` or
`ERR` for another purpose. `atexit` will also set an exit code metric
for the script based on `$?`; if you are calling it manually it accepts
an alternative value as a sole argument.


## Fallbacks

If you want your script to work even when prom.sh is not available, you
can stub out `prom-time` to just run its arguments and `prom` to do
nothing:

```sh
prom() { :; }; prom-time() { shift; "$@"; }
source /usr/local/lib/prometheus/prom.sh || true
```


## Limitations

`prom.sh` does not ensure metric consistency. If you use `prom set` and
`prom inc` with the same name, or a name that conflicts with the format
`prom time` uses, it will generate an inconsistent exposition.
